---
title: Meteor Shortcode#2 - Çok Basit Dinamik Router Oluşturma
author: Emre Keskin
desc: Iron-Router Dynamic Router
---

### Çok Basit Dinamik Router Oluşturma - Iron-Router Dynamic Router

Meteor'da router yapmak için çok kullanılan iron:router paketinde çok basit şekilde dinamik olarak router oluşturmayı kısa kod olarak aşağıda verdim.

### Paket

```
meteor add iron:router

```


### Kullanımı

```javascript
//
// Router Ayarları
//
Router.configure({
    layoutTemplate: 'Layout',
    notFoundTemplate: 'notFound'
});
//
// Eğer userId yoksa yani giriş yapmadı ise Login veya Register girebilsin dedik.
//
Router.onBeforeAction(function() {
  if (!Meteor.userId()) {
    this.render('Login');
    this.layout('Layout');
  } else {
    this.next();
  }
},{except:["Register"]});


//
// Register ve Login için ayrı bir router oluşturdum çünkü onun Layoutu farklı olduğu için
// eğer aynı ise yapmanıza gerek yoktur.
//
Router.route('/Register', function () {
    this.render('Register');
});
Router.route('/Login', function () {
    this.render('Login');
});
//
// Dinamik router burada params.id gelen yazıya göre o templati getiriyor ve render ediyor.
//
Router.route('/:id', function () {
    this.render(this.params.id);
    this.layout('mainLayout');
});
//
// Eğer bulunamaz ise ayarlarda belirttiğimiz gibi bu sayfaya gidiyor
//
Router.route('/notFound', function () {
    this.render('notFound');
});
//
// Default route
//
Router.route('/', function () {
    Router.go('Homepage');
});


```

> - Kütüphane'nin Diğer Özellikleri

[on Github](https://github.com/iron-meteor/iron-router)
[on AtmosphereJS](https://atmospherejs.com/iron/router)
