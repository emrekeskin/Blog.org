---
title: Meteor VeliovGroup/Meteor-Files 
date: 2016-12-10 05:13:13
author: Emre Keskin
desc: Meteor VeliovGroup/Meteor-Files 
---


Meteorda resim yükleme işlemi için kullandığım paket [on Github](https://github.com/VeliovGroup/Meteor-Files/)

# Client Kodu 

- HTML

~~~ html 

<template name="uploadForm">
  {{#with currentUpload}}
    Uploading <b>{{file.name}}</b>:
    <span id="progress">{{progress.get}}%</span>
  {{else}}
    <input id="fileInput" type="file" />
  {{/with}}
</template>


~~~

- JS

~~~ javascript 

Meteor.subscribe('files.images.all');

Template.uploadForm.onCreated(function () {
  this.currentUpload = new ReactiveVar(false);
});

Template.uploadForm.helpers({
  currentUpload: function () {
    return Template.instance().currentUpload.get();
  }
});

Template.uploadForm.events({
  'change #fileInput': function (e, template) {
    if (e.currentTarget.files && e.currentTarget.files[0]) {
      // We upload only one file, in case
      // multiple files were selected
      var upload = Images.insert({
        file: e.currentTarget.files[0],
        streams: 'dynamic',
        chunkSize: 'dynamic'
      }, false);

      upload.on('start', function () {
        template.currentUpload.set(this);
      });

      upload.on('end', function (error, fileObj) {
        if (error) {
          alert('Error during upload: ' + error);
        } else {
          alert('File "' + fileObj.name + '" successfully uploaded');
        }
        template.currentUpload.set(false);
      });

      upload.start();
    }
  }
});


~~~



# Server Kodu 


- publish.js 

~~~ javascript 



this.Images = new Meteor.Files({
  debug: true,
  storagePath:   'public/emre',
  collectionName: 'Images',
  allowClientCode: false, // Disallow remove files from Client
  onBeforeUpload: function (file) {
    // Allow upload files under 10MB, and only in png/jpg/jpeg formats
    if (file.size <= 1024*1024*10 && /png|jpg|jpeg/i.test(file.extension)) {
      return true;
    } else {
      return 'Please upload image, with size equal or less than 10MB';
    }
  }
});


Images.denyClient();
Meteor.publish('files.images.all', function () {
  return Images.find().cursor;
});


~~~



# Resim Dosyasının Link

Resim dosyasının linkini almak için 

~~~ 

return Images.findOne({}).link()

~~~